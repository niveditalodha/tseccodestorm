<?php
$servername = "localhost";
$username = "codestorm";
$password = "12345";

// Create connection
$conn = mysqli_connect("localhost", "codestorm", "12345", "csevents");

// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>TSEC Hackathon Registrations</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
<?php

$query = "select * from hackathon";
$result = mysqli_query($conn, $query);
$counter = 0;

if (mysqli_num_rows($result) > 0) {
    // output data of each row
    $echo_string = '<h2>Registrations</h2><table class="table table-hover"><thead><tr><th>Sr. No.</th><th>ID</th><th>First Name</th><th>Last Name</th><th>Email</th><th>Phone</th><th>College</th><th>Year</th><th>Team Name</th></tr</thead><tbody>';
    while($row = mysqli_fetch_assoc($result)) {
    	$counter++;
	$echo_string .= "<tr><td>" . $counter . "</td><td>" . $row["id"] . "</td><td>" . $row["fname"] . "</td><td>" . $row["lname"] . "</td><td>" . $row["email"] . "</td><td>" . $row["phone"] . "</td><td>" . $row["college"] . "</td><td>" . $row["year"] . "</td><td>" . $row["teamname"] . "</td></tr>";    
    }
    $echo_string .= "</tbody></table>";
    echo $echo_string;
}

mysqli_close($conn);

?>

</div>
</body>
</html>