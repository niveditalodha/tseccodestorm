<?php

// Create functions
function send_error($message)
{
    echo json_encode(array(
        "status" => "error",
        "message" => $message
    ));
    exit();
}

function send_sucess($message)
{
    echo json_encode(array(
        "status" => "ok",
        "message" => $message
    ));
    exit();
}

//$link = mysqli_connect("localhost","codestorm","12345","csevents");
$link = mysqli_connect("localhost","codestorm","12345","csevents");

if (!$link) {
    echo "Error: Unable to connect to MySQL." . PHP_EOL;
    echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
    send_error('Connecting error!');
}

// Check connection
if($link === false){
    send_error('Unable to Register.');
}


$teamname=mysqli_real_escape_string($link, $_POST["team-name"]);
$members=mysqli_real_escape_string($link, $_POST["members-number"]);

$fname1=mysqli_real_escape_string($link, $_POST["member-1-first-name"]);
$lname1=mysqli_real_escape_string($link, $_POST["member-1-last-name"]);
$email1=mysqli_real_escape_string($link, $_POST["member-1-email"]);
$phone1=mysqli_real_escape_string($link, $_POST["member-1-phone"]);
$college1=mysqli_real_escape_string($link, $_POST["member-1-college"]);
$year1=mysqli_real_escape_string($link, $_POST["member-1-year"]);


    $dbsql = "SELECT * from `hackathon` WHERE `teamname`='".$teamname."'";
    $result = mysqli_query($link, $dbsql);
    $result_rows = mysqli_num_rows($result);
    $success_flag = 0;
    if($result_rows > 0) {
        send_error('Team Name already exists, please try another.');
    }
    else
    {
	    $fname1 = mysqli_real_escape_string($link, $_POST["member-1-first-name"]);
		$lname1 = mysqli_real_escape_string($link, $_POST["member-1-last-name"]);
		$email1 = mysqli_real_escape_string($link, $_POST["member-1-email"]);
		$phone1 = mysqli_real_escape_string($link, $_POST["member-1-phone"]);
		$college1 = mysqli_real_escape_string($link, $_POST["member-1-college"]);
		$year1 = mysqli_real_escape_string($link, $_POST["member-1-year"]);
	    $sql = "INSERT INTO `hackathon` (`fname`, `lname`, `email`, `phone`, `college`, `year`, `teamname`) VALUES ('".$fname1."','".$lname1."','".$email1."','".$phone1."','".$college1."','".$year1."','".$teamname."');";

		    if(mysqli_query($link, $sql)){
                $success_flag = 1;
		        $to_email = $email1;
		        $subject = 'CodeStorm Hackathon Registration';
		        $message = 'Thank you for registering. Date : 21st September, Time: 9 AM, Venue: Thadomal Shahani Engineering College, Bandra(W)';
		        $headers = 'From: tseccodestorm@gmail.com';
		        mail($to_email,$subject,$message,$headers);
		    } else{
		        send_error('Registration failed for member 1.');
		    }

	    if($members >= 2)
	    {
	    	$fname2 = mysqli_real_escape_string($link, $_POST["member-2-first-name"]);
			$lname2 = mysqli_real_escape_string($link, $_POST["member-2-last-name"]);
			$email2 = mysqli_real_escape_string($link, $_POST["member-2-email"]);
			$phone2 = mysqli_real_escape_string($link, $_POST["member-2-phone"]);
			$college2 = mysqli_real_escape_string($link, $_POST["member-2-college"]);
			$year2 = mysqli_real_escape_string($link, $_POST["member-2-year"]);
		    $sql = "INSERT INTO `hackathon` (`fname`, `lname`, `email`, `phone`, `college`, `year`, `teamname`) VALUES ('".$fname2."','".$lname2."','".$email2."','".$phone2."','".$college2."','".$year2."','".$teamname."');";
		    	 if(mysqli_query($link, $sql)){
                     $success_flag = 2;
			        $to_email = $email2;
			        $subject = 'CodeStorm Hackathon Registration';
			        $message = 'Thank you for registering. Date : 21st September, Time: 9 AM, Venue: Thadomal Shahani Engineering College, Bandra(W)';
			        $headers = 'From: tseccodestorm@gmail.com';
			        mail($to_email,$subject,$message,$headers);
			    } else{
                     send_error('Registration failed for member 2.');
		    	}
	    }
	    if($members >= 3)
	    {
	    	$fname3 = mysqli_real_escape_string($link, $_POST["member-3-first-name"]);
			$lname3 = mysqli_real_escape_string($link, $_POST["member-3-last-name"]);
			$email3 = mysqli_real_escape_string($link, $_POST["member-3-email"]);
			$phone3 = mysqli_real_escape_string($link, $_POST["member-3-phone"]);
			$college3 = mysqli_real_escape_string($link, $_POST["member-3-college"]);
			$year3 = mysqli_real_escape_string($link, $_POST["member-3-year"]);
		    $sql = "INSERT INTO `hackathon` (`fname`,`lname`,`email`,`phone`,`college`,`year`,`teamname`) VALUES ('".$fname3."','".$lname3."','".$email3."','".$phone3."','".$college3."','".$year3."','".$teamname."');";
		    	 if(mysqli_query($link, $sql)){
                     $success_flag = 3;
			        $to_email = $email3;
			        $subject = 'CodeStorm Hackathon Registration';
			        $message = 'Thank you for registering. Date : 21st September, Time: 9 AM, Venue: Thadomal Shahani Engineering College, Bandra(W)';
			        $headers = 'From: tseccodestorm@gmail.com';
			        mail($to_email,$subject,$message,$headers);
			    } else{
                     send_error('Registration failed for member 3.');
		    	}
	    }
	    if($members >= 4)
	    {
	    	$fname4 = mysqli_real_escape_string($link, $_POST["member-4-first-name"]);
			$lname4 = mysqli_real_escape_string($link, $_POST["member-4-last-name"]);
			$email4 = mysqli_real_escape_string($link, $_POST["member-4-email"]);
			$phone4 = mysqli_real_escape_string($link, $_POST["member-4-phone"]);
			$college4 = mysqli_real_escape_string($link, $_POST["member-4-college"]);
			$year4 = mysqli_real_escape_string($link, $_POST["member-4-year"]);
		    $sql = "INSERT INTO `hackathon` (`fname`,`lname`,`email`,`phone`,`college`,`year`,`teamname`) VALUES ('".$fname4."','".$lname4."','".$email4."','".$phone4."','".$college4."','".$year4."','".$teamname."');";
		    	 if(mysqli_query($link, $sql)){
                     $success_flag = 4;
			        $to_email = $email4;
			        $subject = 'CodeStorm Hackathon Registration';
			        $message = 'Thank you for registering. Date : 21st September, Time: 9 AM, Venue: Thadomal Shahani Engineering College, Bandra(W)';
			        $headers = 'From: tseccodestorm@gmail.com';
			        mail($to_email,$subject,$message,$headers);
			    } else{
                     send_error('Registration failed for member 4.');
		    	}
	    }
	    if ($success_flag != 0) {
		        send_sucess('Successfully registered for the hackathon.');
        }
	}
    // Close connection
    mysqli_close($link);

    ?>